package app.dto.customer;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerReq {
    private String name;
    private String email;
    private Integer age;
}
