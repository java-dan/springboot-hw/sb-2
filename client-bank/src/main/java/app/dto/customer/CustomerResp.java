package app.dto.customer;

import app.models.Account;
import app.models.Employer;
import lombok.Data;

import java.util.List;

@Data
public class CustomerResp {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<Account> accounts;
    private List<Employer> employers;
}
