package app.dto.employer;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployerReq {
    private String name;
    private String address;
}
