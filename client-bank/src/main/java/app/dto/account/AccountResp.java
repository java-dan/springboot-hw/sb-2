package app.dto.account;

import app.models.Currency;
import app.models.Customer;
import lombok.Data;

import java.util.UUID;

@Data
public class AccountResp {
    private Long id;
    private String number = UUID.randomUUID().toString();;
    private Currency currency;
    private Double balance = (double) 0;
    private Customer customer;
}
