package app.dto.account;

import app.models.Currency;
import app.models.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountReq {
    private Currency currency;
    private Customer customer;
}
