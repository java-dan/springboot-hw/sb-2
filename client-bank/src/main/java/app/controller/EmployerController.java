package app.controller;

import app.dto.employer.EmployerResp;
import app.models.Employer;
import app.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employers")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class EmployerController {
    private final EmployerService employerService;

    @GetMapping
    public List<EmployerResp> getAllEmployers() {
        return employerService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Employer> getEmployer(@PathVariable("id") Long id) { return employerService.findById(id); }

    @PostMapping("/create/{customerId}")
    public ResponseEntity<Employer> createEmployer(@PathVariable("customerId") Long cId, @RequestBody Employer e) {
        Employer result = employerService.save(cId, e);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEmployer (@PathVariable Long id){
        return employerService.deleteById(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{employerId}/customers/{customerId}")
    public ResponseEntity<?> deleteEmployerFromCustomer (@PathVariable(value = "employerId") Long employerId, @PathVariable(value = "customerId") Long customerId){
        return employerService.deleteEmployerFromCustomer(customerId, employerId)
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }
}
