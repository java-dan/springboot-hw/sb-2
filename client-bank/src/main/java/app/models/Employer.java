package app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="employers")
@EqualsAndHashCode(callSuper=false)
public class Employer extends AbstractEntity{
    private String name;
    private String address;

    @ManyToMany(mappedBy = "employers")
    @JsonIgnoreProperties("employers")
    private List<Customer> customers;
}