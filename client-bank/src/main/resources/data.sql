INSERT INTO customers (age, email, name) VALUES (27, 'elena@gmail.com', 'Elena');
INSERT INTO customers (age, email, name) VALUES (55, 'max@gmail.com', 'Max');
INSERT INTO customers (age, email, name) VALUES (38, 'gim@gmail.com', 'Gim');

INSERT INTO accounts (balance, currency, number, customer_id) VALUES (4000.00, 'EUR', 'f2962718-8cf6-5040-b44d-f62c042f5d41', 1);
INSERT INTO accounts (balance, currency, number, customer_id) VALUES (38.70, 'USD', '96ad01f4-332b-59b9-bb72-131305c5636a', 1);
INSERT INTO accounts (balance, currency, number, customer_id) VALUES (500.00, 'UAH', 'f2962718-8cf6-5040-b44d-f62c042f5d41', 1);
INSERT INTO accounts (balance, currency, number, customer_id) VALUES (7500.00, 'EUR', '8e39f30d-6402-5d5d-85ac-86ee78b423cf', 2);
INSERT INTO accounts (balance, currency, number, customer_id) VALUES (0.00, 'UAH', 'c359657d-6995-5c3d-b85d-84871637954c', 2);
INSERT INTO accounts (balance, currency, number, customer_id) VALUES (2300.00, 'USD', '87ca6c12-ea4a-4d36-82f4-006bcec5d79c', 3);
--
INSERT INTO employers (address, name) VALUES ('USA', 'Apple');
INSERT INTO employers (address, name) VALUES ('London', 'Samsung');
INSERT INTO employers (address, name) VALUES ('Japan', 'Nokia');

INSERT INTO customers_employers (customers_id, employers_id) VALUES (3, 3);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (2, 3);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (1, 3);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (1,2);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (3, 2);
INSERT INTO customers_employers (customers_id, employers_id) VALUES (1, 1);